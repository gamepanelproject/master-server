const config 		= require('./scripts/utils/Config');

const express 		= require('express');
const https 		= require('https');
const http 			= require('http');
const fs 			= require('fs');
const path 			= require('path');
const async			= require('async');
const helmet		= require('helmet');

const compression 	= require('compression');
const bodyParser 	= require('body-parser');

const logger 		= require('./scripts/utils/Logger');
const DB 			= require('./scripts/utils/MySQL');

const VERSION		= "0.0.1";

var app = express();

function parallel(middlewares) {
	return function (req, res, next) {
		async.each(middlewares, function (mw, cb) {
			mw(req, res, cb);
		}, next);
	};
}

app.use(parallel([
	bodyParser.json(),
	bodyParser.urlencoded({ extended: false }),
	compression(),
	helmet()
]));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
	next();
});

var server;
if(config.ssl_mode) {
	server = https.createServer(options, app).listen(config.ssl_port);
	logger.debug("[GP | Web] HTTP Secure Server Started On " + config.ssl_port);
} else {
	server = http.createServer(app).listen(config.host_port);
	logger.debug("[GP | Web] HTTP Server Started On " + config.host_port);
}

var comm = require('./scripts/comm/server');
comm.setupComm(server);

//===============ROUTES===============
require('./routes/users')(app);
require('./routes/groups')(app);
require('./routes/tickets')(app);
require('./routes/services')(app);

require('./routes/admin/servers')(app);
require('./routes/admin/clusters')(app);
require('./routes/admin/templates')(app);
require('./routes/admin/users')(app);
require('./routes/admin/tickets')(app);
require('./routes/admin/services')(app);

logger.info("[GP] Authorization Server Started! v" + VERSION);

DB.getConnection();
	
module.exports = app;