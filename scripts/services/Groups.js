const Config 	= require('../utils/Config');

const Logger 	= require('../utils/Logger');
const MySQL 	= require('../utils/MySQL');

const Q 		= require('q');

module.exports = {
	getAll: function() {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM groups;', [], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	},
	
	get: function(groupId) {
		var deferred = Q.defer();
	
		MySQL.getConnection().then(function(connection) {
			connection.on('error', function(err) {      
			   deferred.reject(err);
				return;    
			});
			
			connection.query('SELECT * FROM groups WHERE GroupID=?;', [groupId], function(err, rows, fields) {
				connection.release();
				
				if (err) {
					deferred.reject(err);
				} else {
					deferred.resolve(rows);
				}
				
			});
		}, function(err) {
			deferred.reject(err);
		})
		
		return deferred.promise;
	}
};