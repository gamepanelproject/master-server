const WebUtils 	= require('../../scripts/utils/Web');
const Logger 	= require('../../scripts/utils/Logger');

const Templates = require('../../scripts/services/Templates');

module.exports = function(app) {
	
	app.options('/admin/templates', function(req, res) {
		Logger.silly('[WEB | Admin | templates | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/admin/templates', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Admin | templates | GET]');
		
		Templates.getAll().then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'Error getting templates'));
				return
			}
			
			res.json(WebUtils.response(false, 'Success', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Admin | templates | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
}