const WebUtils 	= require('../../scripts/utils/Web');
const Logger 	= require('../../scripts/utils/Logger');

const Services 		= require('../../scripts/services/Services');

module.exports = function(app) {
	
	app.options('/admin/services', function(req, res) {
		Logger.silly('[WEB | Admin | services | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/admin/services', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Admin | services | GET]');
		
		Services.getAllAdmin().then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'Error getting services'));
				return
			}
			
			res.json(WebUtils.response(false, 'Success', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Admin | services | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
}