const WebUtils 	= require('../../scripts/utils/Web');
const Logger 	= require('../../scripts/utils/Logger');

const Users 		= require('../../scripts/services/Users');

module.exports = function(app) {
	
	app.options('/admin/users', function(req, res) {
		Logger.silly('[WEB | Admin | users | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/admin/users', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Admin | users | GET]');
		
		Users.getUsers().then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'Error getting user'));
				return
			}
			
			res.json(WebUtils.response(false, 'Success', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Admin | users/| GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
}