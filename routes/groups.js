const WebUtils 	= require('../scripts/utils/Web');
const Groups 	= require('../scripts/services/Groups');
const Logger 	= require('../scripts/utils/Logger');

module.exports = function(app) {
	
	app.options('/groups', function(req, res) {
		Logger.silly('[WEB | Groups | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/groups', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Groups | GET]');
		
		Groups.getAll().then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'User does not exist'));
				return
			}
			
			res.json(WebUtils.response(false, 'Logged in', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Groups | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
}