const WebUtils 		= require('../scripts/utils/Web');

const Services 		= require('../scripts/services/Services');
const Templates 	= require('../scripts/services/Templates');

const Logger 		= require('../scripts/utils/Logger');
const Communication = require('../scripts/comm/server');

module.exports = function(app) {
	//Get all of a users services
	app.options('/services', function(req, res) {
		Logger.silly('[WEB | Services | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS, PUT, DELETE, UPDATE");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/services', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | Services | GET]');
		
		Services.getAll(req.UserID).then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'Error retriving services'));
				return
			}
			
			res.json(WebUtils.response(false, 'Got services', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Services | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
	
	//Get service details as are from DB
	app.options('/services/:ServiceID', function(req, res) {
		Logger.silly('[WEB | /services/:ServiceID | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/services/:ServiceID', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | /services/:ServiceID | GET]');
		
		Services.get(req.params.ServiceID).then(function(result) {
			if(!result) {
				res.json(WebUtils.response(true, 'Error retriving service'));
				return
			}
			
			res.json(WebUtils.response(false, 'Got service', JSON.stringify(result)));
		}, function(err) {
			Logger.error('[WEB | Services/:ServiceID | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
	
	//Get Service Status from Remote Server
	app.options('/services/:ServiceID/status', function(req, res) {
		Logger.silly('[WEB | /services/:ServiceID/status | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/services/:ServiceID/status', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | /services/:ServiceID/status | GET]');
		
		var remoteServer = Communication.getRemoteServerById(1);
		if(remoteServer) {
			remoteServer.getServiceStatus(req.params.ServiceID).then(function(result) {
				if(!result) {
					res.json(WebUtils.response(true, 'Error retriving service'));
					return
				}
				
				res.json(WebUtils.response(false, 'Got status', JSON.stringify(result)));
			}, function(err) {
				Logger.error('[WEB | Services/:ServiceID/status | GET] Internal Error');
				res.json(WebUtils.response(true, 'Internal error...'));
			});
		} else {
			res.json(WebUtils.response(true, 'Internal error...'));
		}
		
	})
	
	//Install / reinstall service
	app.options('/services/:ServiceID/reinstall', function(req, res) {
		Logger.silly('[WEB | /services/:ServiceID/reinstall | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/services/:ServiceID/reinstall', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | /services/:ServiceID/reinstall | GET]');
		
		Services.get(req.params.ServiceID).then(function(service) {
			if(!service) {
				res.json(WebUtils.response(true, 'Error retriving service'));
				return
			}
			
			var remoteServer = Communication.getRemoteServerById(service.ServerID);
			if(remoteServer) {
				remoteServer.reinstallService(req.params.ServiceID).then(function(result) {
					if(!result) {
						res.json(WebUtils.response(true, 'Error running command'));
						return
					}
					
					res.json(WebUtils.response(false, 'Got result', JSON.stringify(result)));
				}, function(err) {
					Logger.error('[WEB | Services/:ServiceID/reinstall | GET] Internal Error');
					res.json(WebUtils.response(true, 'Internal error...'));
				});
			} else {
				res.json(WebUtils.response(true, 'Remote server offline'));
			}
			
		}, function(err) {
			Logger.error('[WEB | Services/:ServiceID/reinstall | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
	
	app.options('/services/:ServiceID/start', function(req, res) {
		Logger.silly('[WEB | /services/:ServiceID/start | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/services/:ServiceID/start', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | /services/:ServiceID/start | GET]');
		
		Services.get(req.params.ServiceID).then(function(service) {
			if(!service) {
				res.json(WebUtils.response(true, 'Error retriving service'));
				return
			}
			
			var remoteServer = Communication.getRemoteServerById(service.ServerID);
			if(remoteServer) {
				remoteServer.startService(req.params.ServiceID).then(function(result) {
					if(!result) {
						res.json(WebUtils.response(true, 'Error running command'));
						return
					}
					
					res.json(WebUtils.response(false, 'Got result', JSON.stringify(result)));
				}, function(err) {
					Logger.error('[WEB | Services/:ServiceID/start | GET] Internal Error');
					res.json(WebUtils.response(true, 'Internal error...'));
				});
			} else {
				res.json(WebUtils.response(true, 'Remote server offline'));
			}
			
		}, function(err) {
			Logger.error('[WEB | Services/:ServiceID/start | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
	
	app.options('/services/:ServiceID/stop', function(req, res) {
		Logger.silly('[WEB | /services/:ServiceID/stop | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/services/:ServiceID/stop', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | /services/:ServiceID/stop | GET]');
		
		Services.get(req.params.ServiceID).then(function(service) {
			if(!service) {
				res.json(WebUtils.response(true, 'Error retriving service'));
				return
			}
			
			var remoteServer = Communication.getRemoteServerById(service.ServerID);
			if(remoteServer) {
				remoteServer.stopService(req.params.ServiceID).then(function(result) {
					if(!result) {
						res.json(WebUtils.response(true, 'Error running command'));
						return
					}
					
					res.json(WebUtils.response(false, 'Got result', JSON.stringify(result)));
				}, function(err) {
					Logger.error('[WEB | Services/:ServiceID/stop | GET] Internal Error');
					res.json(WebUtils.response(true, 'Internal error...'));
				});
			} else {
				res.json(WebUtils.response(true, 'Remote server offline'));
			}
			
		}, function(err) {
			Logger.error('[WEB | Services/:ServiceID/stop | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
	})
	
	//Get the template for a serviceID
	app.options('/services/:ServiceID/template', function(req, res) {
		Logger.silly('[WEB | /services/:ServiceID/template | GET | OPTIONS]');
		
		res.header("access-control-allow-methods", "GET, OPTIONS");
		res.header("access-control-max-age", 0);
		res.header("Content-Type", "application/json");
		
		res.json('{}');
	});
	app.get('/services/:ServiceID/template', WebUtils.checkToken, function(req, res){
		Logger.debug('[WEB | /services/:ServiceID/template | GET]');
		
		Services.get(req.params.ServiceID).then(function(service) {
			if(!service) {
				res.json(WebUtils.response(true, 'Error retriving service'));
				return
			}
			
			Templates.get(service.TemplateID).then(function(template) {
				if(!template) {
					res.json(WebUtils.response(true, 'Error retriving template'));
					return
				}
				
				res.json(WebUtils.response(false, 'Got template', JSON.stringify(template)));
			}, function(err) {
				Logger.error('[WEB | Services/:ServiceID/template | GET] Internal Error');
				res.json(WebUtils.response(true, 'Internal error...'));
			})
		}, function(err) {
			Logger.error('[WEB | Services/:ServiceID/template | GET] Internal Error');
			res.json(WebUtils.response(true, 'Internal error...'));
		})
		
	})
}